const fs = require("fs/promises");
const path = require("path");

function problem2(fileName) {
  fs.readFile(path.join(__dirname, fileName))

    .then((data) => {
      const fileName1 = "Uppercase.txt";

      return new Promise((resolve, reject) => {
        fs.writeFile(
          path.join(__dirname, fileName1),
          data.toString().toUpperCase()
        )

          .then(() => {
            resolve(fileName1);
            console.log("Uppercase File created");
            return fs.writeFile(
              path.join(__dirname, "filenames.txt"),
              fileName1
            );
          })

          .catch((err) => {
            reject(err);
            console.log(err);
          });
      });
    })

    .then((fileName1) => {
      console.log("Stored Uppercase Filename");
      return fs.readFile(path.join(__dirname, fileName1));
    })

    .then((data) => {
      //Splitting the sentences into an array
      const newData = data.toString().toLowerCase().split(". ");
      const fileName2 = "LowercaseSentences.txt";

      //Data is joined with newline to obtain each sentence in each line
      return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, fileName2), newData.join("\n"))

          .then(() => {
            resolve(fileName2);
            console.log("Created lowercase file from Uppercase file");

            //Filename is stored as nextLine in filenames.txt
            return fs.appendFile(
              path.join(__dirname, "filenames.txt"),
              "\n" + fileName2
            );
          })

          .catch((err) => {
            reject(err);
            console.log(err);
          });
      });
    })

    .then((fileName2) => {
      console.log("Appended lowercase filename");
      return fs.readFile(path.join(__dirname, fileName2));
    })

    .then((data) => {
      //Data is first split into sentences in an array and then joined after sorting in separate lines
      const newData2 = data
        .toString()
        .split("\n")
        .sort((currentSentence, nextSentence) => {
          return currentSentence.localeCompare(nextSentence);
        })
        .join("\n");

      const fileName3 = "SortSentences.txt";

      return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, fileName3), newData2)

          .then(() => {
            resolve(fileName3);
            console.log("Created Sorted file from Lowercase file");
            return fs.appendFile(
              path.join(__dirname, "filenames.txt"),
              "\n" + fileName3
            );
          })

          .catch((err) => {
            reject(err);
            console.log(err);
          });
      });
    })

    .then((fileName3) => {
      console.log("Appended Sorted filename");
      return fs.readFile(path.join(__dirname, "filenames.txt"));
    })

    .then((data) => {
      console.log("Read filenames.txt");
      const dataArray = data.toString().split("\n");
      const deleteArray = [];

      for (let index = 0; index < dataArray.length; index++) {
        deleteArray.push(fs.unlink(path.join(__dirname, dataArray[index])));
      }
      return Promise.all(deleteArray);
    })

    .then(() => {
      console.log("Files Deleted");
    })

    .catch((err) => {
      console.log(err);
    });
}

module.exports = problem2;

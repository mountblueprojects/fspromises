const fs = require("fs/promises");
const path = require("path");

function problem1(directoryName) {
  fs.mkdir(path.join(__dirname, directoryName))

    .then(() => {
      // Giving an array of random file names
      const files = [
        "file1.json",
        "file2.json",
        "file3.json",
        "file4.json",
        "file5.json",
      ];

      const createArray = [];

      for (let index = 0; index < files.length; index++) {
        createArray.push(
          fs.writeFile(
            path.join(__dirname, directoryName, files[index]),
            "Random"
          )
        );
      }

      return new Promise((resolve, reject) => {
        Promise.all(createArray)

          .then(() => {
            resolve(files);
          })

          .catch((err) => {
            reject(err);
            console.log(err);
          });
      });
    })

    .then((files) => {
      console.log("Files Created");
      const deleteArray = [];

      for (let index = 0; index < files.length; index++) {
        deleteArray.push(
          fs.unlink(path.join(__dirname, directoryName, files[index]))
        );
      }

      return Promise.all(deleteArray);
    })

    .then(() => {
      console.log("Files Deleted");
    })

    .catch((err) => {
      console.log(err);
    });
}

module.exports = problem1;
